#!/bin/bash
# Converts given input video to suitable format
#
# Usage:
# :~> avconv <inputvideo> <start_time:hh:mm:ss> <duration:ss>

# Check parameters
if [ $# -eq 0 ]
then
	echo "Usage:"
	echo "$0 <inputvideo> <start_time:hh.mm.ss> <duration:ss>"
	exit 1
fi
if [ $# -eq 1 ]
then
    input=$1
fi
if [ $# -eq 2 ]
then
	echo "Usage:"
	echo "$0 <inputvideo> <start_time:hh.mm.ss> <duration:ss>"
	exit 1
fi
if [ $# -eq 3 ]
then
    input=$1
    start_time=$2
    duration=$2
fi

# Check file
if [ ! -f $1 ]
then
	echo "file not found: $1"
	exit 1
fi

# Convert (use avconf by default) -b:v 1024k removed since bad rate
if hash avconv 2>/dev/null; then
    if [ -n "$duration" ]
    then
       avconv -i "$input" -ss $start_time -t $duration -r 24 -b:v 2048k -vf scale=1280:-1 -ar 32000 -b:a 128k "${1%.*}_converted.avi"
    else
       avconv -i "$input" -r 24 -b:v 2048k -vf scale=1280:-1 -ar 32000 -b:a 128k "${1%.*}_converted.avi"
    fi
else
    if [ -n "$duration" ]
    then
	ffmpeg -i "$input" -ss $start_time -t $duration -r 24 -b:v 2048k -vf scale=1280:-1 -ar 32000 -b:a 128k "${1%.*}_converted.avi"
    else
	ffmpeg -i "$input" -r 24 -b:v 2048k -vf scale=1280:-1 -ar 32000 -b:a 128k "${1%.*}_converted.avi"
    fi
fi
