#!/bin/bash

TEMPDIR="./TMP"

function convert_image_to_video {
    text=`echo $4`
    convert $2 -geometry 1280x $TEMPDIR/convert_image_$1.png
    convert $TEMPDIR/convert_image_$1.png -fill white  -undercolor '#00000080'  -pointsize 36 \
	    -gravity Center \
            -annotate +0+5 "$text"  $TEMPDIR/convert_image_$1.png
    duration=`bc <<<"scale=2; 1 / $3"`
    ffmpeg -loglevel panic -nostdin -y -framerate $duration -i $TEMPDIR/convert_image_$1.png -vf scale="1280:trunc(ow/(16/9)/2)*2" -vcodec libx264 -crf 25 -r 10 -pix_fmt yuv420p $TEMPDIR/convert_video_$1.mp4
}

function convert_video_to_video {
    text=`echo $4`
    ffmpeg -loglevel panic -nostdin -y -ss 00:00:00 -i $2 -vf scale="1280:trunc(ow/(16/9)/2)*2" -t $3 -vcodec libx264 -crf 25 -r 10 -pix_fmt yuv420p $TEMPDIR/no_text_convert_video_$1.mp4 -hide_banner
    ffmpeg -loglevel panic -nostdin -y -i $TEMPDIR/no_text_convert_video_$1.mp4 -vf drawtext="text='$text': fontcolor=white: fontsize=36: box=1: boxcolor=black@0.5: boxborderw=5: x=(w-text_w)/2: y=(h-text_h)/2" -codec:a copy $TEMPDIR/convert_video_$1.mp4
}

#
command_list=$1

# Make temp dir
mkdir -p $TEMPDIR

# Make empty image
convert -size 640x400 xc:black $TEMPDIR/black.png

element_num=0
#while IFS='' read -r line || [[ -n "$line" ]]; do
while IFS='' read -r line; do
    # skip comments and empty lines (EMPTY MUST BE EMPTY!)
    if [[ $line = "#"* ]]; then continue; fi;
    if [[ $line = "" ]]; then continue; fi;

    echo $line

    ((element_num++)) # image num
    element_index=$( printf '%03d' $element_num )

    # break to command and arguments
    command=( $line )
    textline=`echo $line | awk '{for ( i=4; i<=NF; i++ ) print $i " "}'|sed 's/"//g'`
    
    # process
    case ${command[0]} in
	"BLACK" ) convert_image_to_video $element_index $TEMPDIR/black.png ${command[2]} "$textline";;
	"IMAGE" ) convert_image_to_video $element_index ${command[1]} ${command[2]} "$textline";;
	"VIDEO" ) convert_video_to_video $element_index ${command[1]} ${command[2]} "$textline";;
	"AUDIO" ) audio_file=${command[1]};;
	*) echo "Unknown command:"  ${command[0]}
    esac
    unset line
done < "$command_list"

# Concatenate all files
ffmpeg -loglevel panic -y -f concat -safe 0 -i <(for f in ./TMP/convert_video_*.mp4; do echo "file '$PWD/$f'"; done) -c copy output.mp4
# Add audio
if [ -z ${audio_file+x} ]; then
    echo "No audio file";
else
    ffmpeg -y -i output.mp4 -i $audio_file -c:v copy -c:a aac -strict experimental output_with_audio.mp4;
fi;
