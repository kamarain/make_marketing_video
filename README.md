# make_marketing_video

## make_video.sh
Can be used to convert a set of images and videos to a single marketing video, try:

./make_video.sh sgn_video_filelist.txt

## convert_video.sh
Can be used to convert longer videos smaller for make_video.sh to save some space, use:

./convert_video.sh <INPUT> <START_TIME as HH:MM:SS> <DURATION in secs>

